﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            Task1 task1 = new Task1();
            int integerNumber = 0;
            Console.WriteLine("Input integer number: ");
            integerNumber = task1.ParseAndValidateIntegerNumber(Console.ReadLine());
            Console.WriteLine("Inputed integer number is: {0}", integerNumber);

            int firstNumber, secondNumber, multiplicationResult;

            Console.WriteLine("-==Multiplication of numbers==-");
            Console.WriteLine("Input two numbers to multiply.");
            Console.WriteLine("Input first number: ");
            firstNumber = task1.ParseAndValidateIntegerNumber(Console.ReadLine());
            Console.WriteLine("Input second number: ");
            secondNumber = task1.ParseAndValidateIntegerNumber(Console.ReadLine());

            multiplicationResult = task1.Multiplication(firstNumber, secondNumber);
            Console.WriteLine("Multilplication result is: {0}", multiplicationResult);

            Task2 task2 = new Task2();
            bool parsingResult;
            Console.WriteLine("Input integer number: ");
            parsingResult = task2.TryParseNaturalNumber(Console.ReadLine(), out int integerNumber2);

            if (parsingResult)
            {
                Console.WriteLine("Parsing is success!");
                Console.WriteLine("Inputed integer number is: {0}", integerNumber2);
            }
            else
            {
                Console.WriteLine("Parsing failed!");
            }

            List<int> evenNumbers = task2.GetEvenNumbers(-6);

            foreach (int element in evenNumbers)
            {
                Console.WriteLine("{0} elements value is: {1} ", evenNumbers.IndexOf(element) + 1, element.ToString());
            }

            Task3 task3 = new Task3();
            Console.WriteLine("Input number: ");
            task3.TryParseNaturalNumber(Console.ReadLine().Trim(), out int source);
            Console.WriteLine("Input digit to remove: ");
            task3.TryParseNaturalNumber(Console.ReadLine().Trim(), out int digitToRemove);
            string resultString = task3.RemoveDigitFromNumber(source, digitToRemove);
            Console.WriteLine("Source number is {0} and digit for remove is {1} ", source, digitToRemove);
            Console.WriteLine("Result string of number is {0}", resultString);
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            bool result;

            result = Int32.TryParse(source.Trim(), out int intValue);

            if (result)
            {
                Console.WriteLine("Converted '{0}' to {1}.", source, intValue);
            }
            else
            {
                throw new ArgumentException("Unable to convert " + source + " !");
            }

            return intValue;
        }

        public int Multiplication(int num1, int num2)
        {
            int result = 0;

            for (int i = 0; i < Math.Abs(num2); i++)
            {
                result += Math.Abs(num1);
            }

            if ((num1 >= 0) && (num2 >= 0))
            {
                result *= 1;
            }
            else if(((num1 < 0) && (num2 >= 0)) || (num1 >= 0) && (num2 < 0))
            {
                result *= -1;
            }

            return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool parsingResult = Int32.TryParse(input.Trim(), out result);

            if (parsingResult)
            {
                Console.WriteLine("Converted '{0}' to {1}.", input, result);
                if (result >= 0)
                {
                    parsingResult = true;
                    Console.WriteLine("Inputed number is natural.");
                }
                else
                {
                    parsingResult = false;
                    Console.WriteLine("Inputed number isn't natural.");
                }
            }
            else
            {
                Console.WriteLine("Unable to convert '{0}'.", input);
            }

            return parsingResult;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            bool isNaturalNumber = (naturalNumber >= 0);

            while (!isNaturalNumber)
            {
                Console.WriteLine("Inputed number isn't natural! Try again.");
                isNaturalNumber = TryParseNaturalNumber(Console.ReadLine().Trim(), out naturalNumber);
            }

            List<int> evenNumbers = new List<int>();
            int currentNumber = 0;

            for(int i = 0; i < naturalNumber; i++)
            {
                evenNumbers.Add(currentNumber);
                currentNumber += 2;
            }

            return evenNumbers;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            Task2 task2 = new Task2();
            return task2.TryParseNaturalNumber(input, out result);
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            bool isNaturalNumber = (source >= 0);

            while (!isNaturalNumber)
            {
                Console.WriteLine("Inputed source number isn't natural! Try again.");
                isNaturalNumber = TryParseNaturalNumber(Console.ReadLine().Trim(), out source);
            }

            while (!isNaturalNumber || (digitToRemove < 0) || (digitToRemove > 9))
            {
                Console.WriteLine("Inputed string not a digit! Try again.");
                isNaturalNumber = TryParseNaturalNumber(Console.ReadLine().Trim(), out digitToRemove);
            }

            string sourceString = source.ToString();
            bool isDigitParsed = Char.TryParse(digitToRemove.ToString(), out char digToRemove);

            int indexOfDigit = sourceString.IndexOf(digToRemove);

            while (isDigitParsed && (indexOfDigit >= 0))
            {
                sourceString = sourceString.Remove(indexOfDigit, 1);
                indexOfDigit = sourceString.IndexOf(digToRemove);
            }

            return sourceString;
        }
    }
}
